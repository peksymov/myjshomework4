alert('Функция, которая является свойством обьекта, называется методом этого обьекта. Метод позволяет обьекту выполнять какие-либо действия.')
function createNewUSer() {
    const userFirstName = prompt('Enter name');
    const userLastName = prompt('Enter LastName');
    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
    }
    return newUser;
}
const newUser = createNewUSer();
console.log(newUser);
console.log(newUser.getLogin());
